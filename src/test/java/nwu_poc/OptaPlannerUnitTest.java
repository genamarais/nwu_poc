
package nwu_poc;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.BeforeClass;
import org.junit.Test;

import com.psybergate.nwu.poc.domain.person.Student;
import com.psybergate.nwu.poc.domain.placement.StudentPlacement;
import com.psybergate.nwu.poc.domain.requirement.Subject;
import com.psybergate.nwu.poc.domain.serviceprovider.PlacementOpportunity;
import com.psybergate.nwu.poc.domain.serviceprovider.TrainingPartner;
import com.psybergate.nwu.poc.problemsetup.RandomProblemDomainSetup;
import com.psybergate.nwu.poc.solution.PlacementSolver;
import com.psybergate.nwu.poc.solution.PlannedStudentPlacements;

public class OptaPlannerUnitTest {

	static PlannedStudentPlacements solvedPlacementSchedule;

	@BeforeClass
	public static void setUp() {
		PlannedStudentPlacements unsolvedPlacementSchedule = setupProblemDomain();
		PlacementSolver solver = new PlacementSolver();
		solvedPlacementSchedule = solver.placeStudentsAtOpportunities(unsolvedPlacementSchedule);
	}
	
	private static PlannedStudentPlacements setupProblemDomain() {
		RandomProblemDomainSetup randomProblemDomain = new RandomProblemDomainSetup();
		randomProblemDomain.setNumStudents(1000);
		randomProblemDomain.setNumTrainingPartners(500);
		randomProblemDomain.setNumOfSubjects(7);
		randomProblemDomain.setMaxSubjectsPerStudent(2);
		randomProblemDomain.setNumPartnerChoicesPerStudent(3);
		
		return randomProblemDomain.setupAvailableOpportunities();
	}

	@Test
	public void allPlacedStudentsHaveRequiredSubject() {
		List<StudentPlacement> studentPlacements = solvedPlacementSchedule.getStudentPlacements();

		for (StudentPlacement studentPlacement : studentPlacements) {
			if (isStudentPlaced(studentPlacement)) {
				Student student = studentPlacement.getStudent();
				Subject requiredSubject = studentPlacement.getPlacementOpportunity().getRequiredSubject();
				assertTrue(student.hasRequiredSubject(requiredSubject));
			}
		}
	}

	@Test
	public void placementOpportunityNotAssignedTwice() {
		List<StudentPlacement> studentPlacements = solvedPlacementSchedule.getStudentPlacements();
		Set<PlacementOpportunity> opportunitiesFilled = new HashSet<PlacementOpportunity>();
		for (StudentPlacement studentPlacement : studentPlacements) {
			if (isStudentPlaced(studentPlacement)) {
				PlacementOpportunity placementOpportunity = studentPlacement.getPlacementOpportunity();
				assertTrue(!opportunitiesFilled.contains(placementOpportunity));
				opportunitiesFilled.add(placementOpportunity);
			}
		}
	}

	@Test
	public void confirmPreferredPartnerFalloutCount() {
		int confirmedFallout = 0;
		for (StudentPlacement studentPlacement : solvedPlacementSchedule.getStudentPlacements()) {
			if (isStudentPlaced(studentPlacement)) {
				Student student = studentPlacement.getStudent();
				TrainingPartner partner = studentPlacement.getPlacementOpportunity().getPartner();
				if (!student.hasAsPreferredPartner(partner)) {
					confirmedFallout++;
				}
			}
		}
		System.out.println("Preferred Partner Fallout: " + confirmedFallout);
 		assertEquals( confirmedFallout, solvedPlacementSchedule.getPreferedPartnerFallout().size());
	}
	
	@Test
	public void confirmUnplacedFalloutCount() {
		int confirmedFallout = 0;
		for (StudentPlacement studentPlacement : solvedPlacementSchedule.getStudentPlacements()) {
			if (!isStudentPlaced(studentPlacement)) {
					confirmedFallout++;
			}
		}
		System.out.println("Unplaced Student Fallout: " + confirmedFallout);
		assertEquals(confirmedFallout, solvedPlacementSchedule.getUnplacedFallout().size());
	}


	private boolean isStudentPlaced(StudentPlacement studentPlacement) {
		return studentPlacement.getPlacementOpportunity() != null;
	}

}
