package com.psybergate.nwu.poc.solution;

import org.optaplanner.benchmark.api.PlannerBenchmark;
import org.optaplanner.benchmark.api.PlannerBenchmarkFactory;
import org.optaplanner.core.api.solver.Solver;
import org.optaplanner.core.api.solver.SolverFactory;

public class PlacementSolver {
	
	private static String businessRulesConfig = "placementScheduleSolverConfigDrools.xml";
	
	private static String benchmarkConfig = "placementBenchmark.xml";

	public PlannedStudentPlacements placeStudentsAtOpportunities(PlannedStudentPlacements unsolvedPlacementSchedule) {
		Solver<PlannedStudentPlacements> solver = getSolverFactory().buildSolver();
		return solver.solve(unsolvedPlacementSchedule);
	}
	
	public void benchmark(PlannedStudentPlacements... unsolvedPlacementSchedules) {
		PlannerBenchmarkFactory benchmarkFactory = PlannerBenchmarkFactory.createFromXmlResource(benchmarkConfig);
        PlannerBenchmark plannerBenchmark = benchmarkFactory.buildPlannerBenchmark(unsolvedPlacementSchedules);
        plannerBenchmark.benchmarkAndShowReportInBrowser();
	}

	private SolverFactory<PlannedStudentPlacements> getSolverFactory() {
		return SolverFactory.createFromXmlResource(businessRulesConfig);
	}

}
