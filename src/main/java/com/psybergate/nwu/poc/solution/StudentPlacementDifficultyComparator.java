package com.psybergate.nwu.poc.solution;

import java.io.Serializable;
import java.util.Comparator;

import org.apache.commons.lang3.builder.CompareToBuilder;

import com.psybergate.nwu.poc.domain.placement.StudentPlacement;
import com.psybergate.nwu.poc.domain.serviceprovider.PlacementOpportunity;

public class StudentPlacementDifficultyComparator implements Comparator<StudentPlacement>, Serializable {

	private static final long serialVersionUID = -3164542066518879119L;

	@Override
	public int compare(StudentPlacement a, StudentPlacement b) {

		if (a.getPlacementOpportunity() != null && b.getPlacementOpportunity() != null) {
			int studentYearA = a.getStudent().getYear();
			int studentYearB = b.getStudent().getYear();

			PlacementOpportunity placementOpportunityA = a.getPlacementOpportunity();
			PlacementOpportunity placementOpportunityB = a.getPlacementOpportunity();

			int studentPreferenceA = a.getStudent().partnerChoiceNumber(placementOpportunityA.getPartner());
			int studentPreferenceB = b.getStudent().partnerChoiceNumber(placementOpportunityB.getPartner());

			return new CompareToBuilder().append(studentYearB, studentYearA) // Descending order
					.append(studentPreferenceB, studentPreferenceA) // Descending order
					.toComparison();
		}
		return 0;
	}

}