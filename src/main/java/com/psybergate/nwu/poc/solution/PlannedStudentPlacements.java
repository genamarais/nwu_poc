package com.psybergate.nwu.poc.solution;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.optaplanner.core.api.domain.solution.PlanningEntityCollectionProperty;
import org.optaplanner.core.api.domain.solution.PlanningScore;
import org.optaplanner.core.api.domain.solution.PlanningSolution;
import org.optaplanner.core.api.domain.solution.drools.ProblemFactCollectionProperty;
import org.optaplanner.core.api.domain.valuerange.ValueRangeProvider;
import org.optaplanner.core.api.score.buildin.hardsoft.HardSoftScore;

import com.psybergate.nwu.poc.domain.person.Student;
import com.psybergate.nwu.poc.domain.placement.StudentPlacement;
import com.psybergate.nwu.poc.domain.serviceprovider.PlacementOpportunity;
import com.psybergate.nwu.poc.domain.serviceprovider.TrainingPartner;

import lombok.Getter;
import lombok.Setter;

@PlanningSolution
public class PlannedStudentPlacements {

	@PlanningEntityCollectionProperty
	@Getter
	@Setter
	private List<StudentPlacement> studentPlacements = new ArrayList<>();

	@Setter
	private List<PlacementOpportunity> placementOpportunities = new ArrayList<>();

	@Setter
	private List<Student> students = new ArrayList<>();

	@PlanningScore
	@Getter
	@Setter
	private HardSoftScore score;
	
	private List<Student> preferedPartnerFallout = new ArrayList<Student>();

	private List<Student> unplacedFallout  = new ArrayList<Student>();	
	
	
	@ValueRangeProvider(id = "availablePlacementOpportunities")
	public List<PlacementOpportunity> getPlacementOpportunities() {
		return placementOpportunities;
	}
	
	@ProblemFactCollectionProperty
	public List<Student> getStudents() {
		return students;
	}

	public void makeManualPlacements(Set<StudentPlacement> manualPlacements) {
		studentPlacements.addAll(manualPlacements);
	}

	public List<Student> getPreferedPartnerFallout() {
		if (falloutUndetermined()) {
			determineFallout();
		}
		return preferedPartnerFallout;
	}

	public List<Student> getUnplacedFallout() {
		if (falloutUndetermined()) {
			determineFallout();
		}
		return unplacedFallout;
	}

	private boolean isStudentPlaced(StudentPlacement placement) {
		return placement.getPlacementOpportunity() != null;
	}

	private boolean falloutUndetermined() {
		return preferedPartnerFallout == null || unplacedFallout == null;
	}

	private void determineFallout() {
		preferedPartnerFallout = new ArrayList<>();
		unplacedFallout = new ArrayList<>();

		
		for (StudentPlacement studentPlacement : studentPlacements) {
			Student student = studentPlacement.getStudent();
			if (isStudentPlaced(studentPlacement)) {
				TrainingPartner partner = studentPlacement.getPlacementOpportunity().getPartner();
				if (!student.hasAsPreferredPartner(partner)) {
					preferedPartnerFallout.add(student);
				}
			}else {
				unplacedFallout.add(student);
			}
		}
	}

}
