package com.psybergate.nwu.poc.problemsetup;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.psybergate.nwu.poc.domain.person.Student;
import com.psybergate.nwu.poc.domain.placement.StudentPlacement;
import com.psybergate.nwu.poc.domain.requirement.Subject;
import com.psybergate.nwu.poc.domain.serviceprovider.PlacementOpportunity;
import com.psybergate.nwu.poc.domain.serviceprovider.TrainingPartner;
import com.psybergate.nwu.poc.solution.PlannedStudentPlacements;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
public class RandomProblemDomainSetup {
	
	@Getter
	@Setter
	private int numStudents;
	
	@Getter
	@Setter
	private int numOfSubjects = 7;
	
	@Getter
	@Setter
	private int maxSubjectsPerStudent = 2;
	
	@Getter
	@Setter
	private int numTrainingPartners = 1000;
	
	@Getter
	@Setter
	private int numPartnerChoicesPerStudent = 3;

	@Getter
	@Setter
	private int primarySubjectMaxOpportunities = 10;
	
	@Getter
	@Setter
	private int primarySubjectMinOpportunities = 7;
	
	@Getter
	@Setter
	private int secondarySubjectMaxOpportunities = 5;
	
	@Getter
	@Setter
	private int secondarySubjectMinOpportunities = 10;

	private Map<Subject, Set<TrainingPartner>> partnersOfferingSubject = new HashMap<>();

	
	public RandomProblemDomainSetup(int numStudents, int numTrainingPartners) {
		super();
		this.numStudents = numStudents;
		this.numTrainingPartners = numTrainingPartners;
	}
	

	public PlannedStudentPlacements setupAvailableOpportunities() {
		PlannedStudentPlacements unsolvedPlacementSchedule = new PlannedStudentPlacements();
		unsolvedPlacementSchedule.getPlacementOpportunities().addAll(placementOpportunites());
		unsolvedPlacementSchedule.getStudents().addAll(availableStudents());
		unsolvedPlacementSchedule.getStudentPlacements().addAll(studentPlacements());
		return unsolvedPlacementSchedule;
	}

	private List<PlacementOpportunity> placementOpportunites() {
		List<PlacementOpportunity> placementOpportunities = new ArrayList<>();
		
		int placementId = 0;
		
		for (int partnerId = 0; partnerId < numTrainingPartners; partnerId++) {
			TrainingPartner partner = new TrainingPartner(partnerId);

			Subject primarySubject = randomSubject();
			Subject secondarySubject = randomSubject();

			while (primarySubject.equals(secondarySubject)) {
				secondarySubject = randomSubject();
			}

			registerPartnerAsOfferingSubject(partner, primarySubject);
			registerPartnerAsOfferingSubject(partner, secondarySubject);

			partner.addOpportunity(primarySubject, numPlacementsAvailableForPrimarySubject());
			partner.addOpportunity(secondarySubject, numPlacementsAvailableForSecondarySubject());

			List<PlacementOpportunity> partnerOpportunities = partner.createPlacementOpportunities();
			
			for (PlacementOpportunity opportunity : partnerOpportunities) {
				opportunity.setId(placementId);
				placementOpportunities.add(opportunity);
				placementId++;
			}
		}
		return placementOpportunities;
	}

	private void registerPartnerAsOfferingSubject(TrainingPartner partner, Subject subject) {
		if (!partnersOfferingSubject.containsKey(subject)) {
			partnersOfferingSubject.put(subject, new HashSet<>());
		}
		partnersOfferingSubject.get(subject).add(partner);
	}

	private int numPlacementsAvailableForSecondarySubject() {
		return randomNumberInRange(secondarySubjectMaxOpportunities, primarySubjectMaxOpportunities);
	}

	private int numPlacementsAvailableForPrimarySubject() {
		return randomNumberInRange(primarySubjectMinOpportunities, primarySubjectMaxOpportunities);
	}

	private List<StudentPlacement> studentPlacements() {
		List<StudentPlacement> placements = new ArrayList<>();
		int id = 0;

		
		for (Student student : availableStudents()) {
			StudentPlacement placement = new StudentPlacement(student);
			placement.setId(id);
			placements.add(placement );
		}

		return placements;
	}

	private List<Student> availableStudents() {
		List<Student> students = new ArrayList<>();

		for (int id = 0; id < numStudents; id++) {
			Student student = new Student(id);

			addSubjects(student);
			student.setYear(randomYear());

			Set<TrainingPartner> possibleTrainingPartners = new HashSet<>();

			student.getSubjects()
					.forEach(subject -> possibleTrainingPartners.addAll(partnersOfferingSubject.get(subject)));

			List<TrainingPartner> preferredPartners = new ArrayList<>();
			while (preferredPartners.size() <= numPartnerChoicesPerStudent) {
				preferredPartners.add(randomPossiblePartner(possibleTrainingPartners));
			}
			
			student.setPreferredPartners(preferredPartners);

			students.add(student);
		}
		return students;
	}

	private TrainingPartner randomPossiblePartner(Set<TrainingPartner> possibleTrainingPartners) {
		TrainingPartner[] trainingPartners = possibleTrainingPartners.toArray(new TrainingPartner[0]);
		int possbilities = trainingPartners.length -1;
		return trainingPartners[randomNumberInRange(0, possbilities)];
	}

	private void addSubjects(Student student) {
		while (student.getSubjects().size() < maxSubjectsPerStudent) {
			student.getSubjects().add(randomSubject());
		}
	}

	private Subject randomSubject() {
		int subjectNumber = randomNumberInRange(0, numOfSubjects);
		return new Subject(subjectNumber);
	}

	private Integer randomYear() {
		return randomNumberInRange(0, 4);
	}

	public int randomNumberInRange(int min, int max) {
		return (int) ((Math.random() * ((max - min) + 1)) + min);
	}


}
