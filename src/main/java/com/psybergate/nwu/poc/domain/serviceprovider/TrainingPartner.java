package com.psybergate.nwu.poc.domain.serviceprovider;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.psybergate.nwu.poc.domain.requirement.Subject;

import lombok.Getter;
import lombok.Setter;

public class TrainingPartner {

	@Getter
	@Setter
	private Integer id;
	
	@Getter
	@Setter
	private Map<Subject, Integer> availableStudentOpportunities = new HashMap<>();
	
	public TrainingPartner(int id) {
		this.id = id;
	}

	public void addOpportunity(Subject subject, Integer availableSpaces) {
		availableStudentOpportunities.put(subject, availableSpaces);
	}

	public List<PlacementOpportunity> createPlacementOpportunities() {
		List<PlacementOpportunity> studentPlacementOpportunities = new ArrayList<>();
		
		for (Entry<Subject, Integer> opportunity : availableStudentOpportunities.entrySet()) {
			Subject requiredSubject = opportunity.getKey();
			Integer availableSpaces = opportunity.getValue();

			for (int i = 0; i < availableSpaces; i++) {
				studentPlacementOpportunities.add(new PlacementOpportunity(this, requiredSubject));
			}
		}
		return studentPlacementOpportunities;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TrainingPartner other = (TrainingPartner) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
}
