package com.psybergate.nwu.poc.domain.person;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
public class Person {

	@Getter
	@Setter
	private Integer casNumber;

	@Getter
	@Setter
	private String name;

	@Getter
	@Setter
	private String surname;
	

	public Person(int id) {
		this(id, "", "");
		
	}

	public Person(int casNumber, String name, String surname) {
		this.casNumber = casNumber;
		this.name = name;
		this.surname = surname;
	}
	
	public Integer getId() {
		return this.casNumber;
	}
	
	public void setId(int id) {
		this.casNumber = id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((casNumber == null) ? 0 : casNumber.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Person other = (Person) obj;
		if (casNumber == null) {
			if (other.casNumber != null)
				return false;
		} else if (!casNumber.equals(other.casNumber))
			return false;
		return true;
	}

}
