package com.psybergate.nwu.poc.domain.person;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.psybergate.nwu.poc.domain.requirement.Subject;
import com.psybergate.nwu.poc.domain.serviceprovider.TrainingPartner;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
public class Student extends Person {

	@Getter
	@Setter
	private int year;

	@Getter
	@Setter
	private Set<Subject> subjects = new HashSet<>();

	@Getter
	@Setter
	private List<TrainingPartner> preferredPartners = new ArrayList<>();
	
	public Student(int id) {
		super(id);
	}

	public Student(int id, String firstName, String lastName) {
		super(id, firstName, lastName);
	}

	public boolean hasRequiredSubject(Subject subject) {
		return subjects.contains(subject);
	}

	public boolean hasAsPreferredPartner(TrainingPartner partner) {
		return preferredPartners.contains(partner);
	}

	public int partnerChoiceNumber(TrainingPartner partner) {
		return preferredPartners.indexOf(partner) + 1;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + getCasNumber();
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Student other = (Student) obj;
		if (getCasNumber() != other.getCasNumber())
			return false;
		return true;
	}



}
