package com.psybergate.nwu.poc.domain.requirement;

import lombok.Getter;
import lombok.Setter;

public class Subject {
	
	@Getter
	@Setter
	private Integer id;

	public Subject(Integer id) {
		this.id = id;
	}
	
	@Override
	public String toString() {
		return "Subject [" + getId() + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Subject other = (Subject) obj;
		if (id != other.id)
			return false;
		return true;
	}
		
	
}
