package com.psybergate.nwu.poc;

import com.psybergate.nwu.poc.problemsetup.RandomProblemDomainSetup;
import com.psybergate.nwu.poc.solution.PlacementSolver;
import com.psybergate.nwu.poc.solution.PlannedStudentPlacements;

public class RunBenchmarkMain {

	public static void main(String[] args) {
		PlannedStudentPlacements first = new RandomProblemDomainSetup(1000,500).setupAvailableOpportunities();
		PlannedStudentPlacements second = new RandomProblemDomainSetup(5000,1000).setupAvailableOpportunities();
		PlannedStudentPlacements third = new RandomProblemDomainSetup(10000,2000).setupAvailableOpportunities();
		
		PlacementSolver solver = new PlacementSolver();
		solver.benchmark(first, second, third);
	}
}
